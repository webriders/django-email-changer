import os
import email_changer
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-email-changer',
    version=email_changer.__version__,
    packages=find_packages(),
    include_package_data=True,
    description='Simple application that gives ability to change email through confirmation',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
    author='Dmytro Popovych',
    author_email='drudim.ua@gmail.com',
    install_requires=[
        'Django>=1.6',
    ]
)
