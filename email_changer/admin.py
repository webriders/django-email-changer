from django.contrib import admin
from models import EmailChangeRequest


class EmailChangeRequestAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'created', 'is_confirmed')
    raw_id_fields = ('user', )


admin.site.register(EmailChangeRequest, EmailChangeRequestAdmin)
