from django.contrib.auth import get_user_model
from django.core import mail
from django.contrib.sites.models import Site
from django.test import TestCase
from models import EmailChangeRequest


__all__ = ['EmailChangeRequestTest']


class EmailChangeRequestTest(TestCase):
    def test_workflow(self):
        UserModel = get_user_model()

        if UserModel.USERNAME_FIELD == 'email':
            user = get_user_model().objects.create_user(email="old_email@example.com")
        else:
            user = get_user_model().objects.create_user(username="drudim", email="old_email@example.com")

        Site(domain="site.example.com", name="Fake site").save()

        # Request email changing
        EmailChangeRequest.objects.add_change_request(user, "first_new_email@example.com")
        change_request = EmailChangeRequest.objects.add_change_request(user, "second_new_email@example.com")

        # While changing not confirmed, old email must be defined
        user = UserModel.objects.get(pk=user.pk)  # Refresh value from db
        self.assertEqual(user.email, "old_email@example.com")

        # Check email sending
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].to, ["first_new_email@example.com"])
        self.assertEqual(mail.outbox[1].to, ["second_new_email@example.com"])

        # Test current change request for user
        self.assertEqual(EmailChangeRequest.objects.current_for_user(user), change_request)

        # Confirm email changing
        EmailChangeRequest.objects.confirm_change_request(change_request.confirmation_key)

        # Test email changing
        user = UserModel.objects.get(pk=user.pk)  # Refresh value from db
        change_request = EmailChangeRequest.objects.get(id=change_request.id)  # Refresh value from db
        self.assertEqual(user.email, "second_new_email@example.com")
        self.assertTrue(change_request.is_expired())
        self.assertIsNone(EmailChangeRequest.objects.current_for_user(user))

        # Delete expired
        EmailChangeRequest.objects.remove_expired_requests()
        self.assertEqual(EmailChangeRequest.objects.count(), 0)
