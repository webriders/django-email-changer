from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView
from django.utils.translation import ugettext_lazy as _
from models import EmailChangeRequest


__all__ = ['email_change_request_confirmation_view']


class EmailChangeRequestConfirmation(RedirectView):
    def get(self, request, *args, **kwargs):
        confirmation_key = kwargs.get('confirmation_key')
        changed = EmailChangeRequest.objects.confirm_change_request(confirmation_key)
        if changed:
            messages.info(request, _("Email was changed"))
        else:
            messages.info(request, _("This confirmation key is expired"))
        return super(EmailChangeRequestConfirmation, self).get(request, *args, **kwargs)

    def get_redirect_url(self, **kwargs):
        return getattr(settings, 'EMAIL_CHANGER_PROFILE_URL', settings.LOGIN_REDIRECT_URL)


email_change_request_confirmation_view = login_required(EmailChangeRequestConfirmation.as_view())

