from datetime import timedelta, datetime
import hashlib
import random
import re
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.transaction import atomic
from django.template.loader import render_to_string
from django.utils import timezone


SHA1_RE = re.compile('^[a-f0-9]{40}$')
EMAIL_CONFIRMATION_DAYS = getattr(settings, 'EMAIL_CONFIRMATION_DAYS', 7)


class EmailChangeRequestManager(models.Manager):
    """
    API to manipulate with email changing
    """
    @atomic
    def add_change_request(self, user, new_email):
        """
        Sends email with confirmation link
        :param user: User instance, who request changing of email
        :param new_email: New email address
        """
        confirmation_key = self._generate_confirmation_key(user)

        try:
            change_request = self.get(user=user)
            change_request.email = new_email
            change_request.created = datetime.now()
            change_request.confirmation_key = confirmation_key
            change_request.is_confirmed = False
            change_request.save()
        except EmailChangeRequest.DoesNotExist:
            change_request = self.create(user=user, email=new_email, confirmation_key=confirmation_key)

        change_request.send_email()
        return change_request

    def confirm_change_request(self, confirmation_key):
        """
        Sets new email for user and mark change request as confirmed
        :param confirmation_key: Secret confirmation key
        """
        if SHA1_RE.search(confirmation_key):
            try:
                change_request = self.get(confirmation_key=confirmation_key, is_confirmed=False)
            except self.model.DoesNotExist:
                return False
            if not change_request.is_expired():
                user = change_request.user
                user.email = change_request.email
                user.save()
                change_request.confirm()
                return True
        return False

    def remove_expired_requests(self):
        """
        Removes expired request
        """
        expiration_timedelta = timedelta(days=EMAIL_CONFIRMATION_DAYS)
        self.filter(Q(is_confirmed=True) | Q(created__lt=datetime.now()-expiration_timedelta)).delete()

    def current_for_user(self, user):
        """
        Return active change request for given user
        :param user: User instance
        """
        try:
            change_request = self.get(user=user)
            if not change_request.is_expired():
                return change_request
        except EmailChangeRequest.DoesNotExist:
            return None

    def _generate_confirmation_key(self, user):
        """
        Algorithm from django registration application
        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = user.get_username()
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        return hashlib.sha1(salt+username).hexdigest()


class EmailChangeRequest(models.Model):
    """
    Model that store information about email change requests from user
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, unique=True)  # Who request email changing
    email = models.EmailField()  # New email
    confirmation_key = models.CharField(max_length=40)  # Secret confirmation key
    created = models.DateTimeField(auto_now_add=True)  # Datetime when request added
    is_confirmed = models.BooleanField(default=False)

    objects = EmailChangeRequestManager()

    def is_expired(self):
        """
        Determines change request status
        """
        expiration_timedelta = timedelta(days=EMAIL_CONFIRMATION_DAYS)
        return self.is_confirmed or (self.created + expiration_timedelta) <= timezone.now()

    def send_email(self):
        """
        Send confirmation email to the new user email address
        :param site: Site instance, from which we must send email
        """
        context = {
            'confirmation_key': self.confirmation_key,
            'expiration_days': EMAIL_CONFIRMATION_DAYS,
            'old_email': self.user.email,
            'new_email': self.email,
            'site': Site.objects.get_current(),
            'path': reverse('email_changer:confirmation', kwargs=dict(confirmation_key=self.confirmation_key))
        }

        subject = render_to_string('email_changer/confirmation_email_subject.txt', context)
        subject = ''.join(subject.splitlines())  # Email subject *must not* contain newlines
        message = render_to_string('email_changer/confirmation_email.txt', context)
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [self.email])

    def confirm(self):
        self.is_confirmed = True
        self.save()
