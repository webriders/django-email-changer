from django.conf.urls import patterns, url


urlpatterns = patterns(
    'email_changer.views',

    url(r'^(?P<confirmation_key>\w+)/$', 'email_change_request_confirmation_view',
        name='confirmation')
)