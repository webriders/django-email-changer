====================
Django email changer
====================

django-email-changer is a simple way to change users email through confirmation

Installation
------------

1. Add "email_changer" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'email_changer',
    )

2. Include the cashbox URLconf in your project urls.py like this::

    url(r'^email-changer/', include('email_changer.urls', namespace='email_changer')),

3. Run `python manage.py migrate` to create the email changer models or `python manage.py syncdb` if you don't use South

4. Don't forget to setup default django site


API description
---------------

Send email with confirmation link::

    EmailChangeRequest.objects.add_change_request(user, new_email)


Return active change request for given user::

    EmailChangeRequest.objects.current_for_user(user)

